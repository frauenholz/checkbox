/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Forms,
    NodeBlock,
    Slots,
    conditions,
    definition,
    each,
    editor,
    metadata,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { ICalculatorMetadata } from "tripetto-block-calculator";
import { CheckboxCondition } from "./conditions/checkbox";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";

/** Assets */
import ICON_CHECKED from "../../assets/checked.svg";
import ICON_UNCHECKED from "../../assets/unchecked.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON_CHECKED,
    alias: "checkbox",
    get label() {
        return pgettext("block:checkbox", "Checkbox (single)");
    },
})
export class Checkbox extends NodeBlock {
    checkboxSlot!: Slots.Boolean;

    @definition("number", "optional")
    scoreForTrue?: number;

    @definition("number", "optional")
    scoreForFalse?: number;

    @metadata("calculator")
    get calculator(): ICalculatorMetadata {
        return {
            checked: {
                scores: [
                    {
                        reference: true,
                        score: this.scoreForTrue,
                    },
                    {
                        reference: false,
                        score: this.scoreForFalse,
                    },
                ],
            },
        };
    }

    @slots
    defineSlot(): void {
        this.checkboxSlot = this.slots.static({
            type: Slots.Boolean,
            reference: "checked",
            label: pgettext("block:checkbox", "Checkbox"),
            exchange: [
                "required",
                "alias",
                "exportable",
                "labelForTrue",
                "labelForFalse",
            ],
        });

        if (!this.checkboxSlot.labelForTrue) {
            this.checkboxSlot.labelForTrue = pgettext(
                "block:checkbox",
                "Checked"
            );
        }

        if (!this.checkboxSlot.labelForFalse) {
            this.checkboxSlot.labelForFalse = pgettext(
                "block:checkbox",
                "Not checked"
            );
        }
    }

    @editor
    defineEditor(): void {
        this.editor.name(false, false);
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.options();
        this.editor.required(
            this.checkboxSlot,
            pgettext("block:checkbox", "Checkbox needs to be checked")
        );
        this.editor.visibility();

        const defaultLabelForTrue = pgettext("block:checkbox", "Checked");
        const defaultLabelForFalse = pgettext("block:checkbox", "Not checked");
        const scoreForTrue = new Forms.Numeric(
            Forms.Numeric.bind(this, "scoreForTrue", undefined)
        ).label(this.checkboxSlot.labelForTrue || defaultLabelForTrue);
        const scoreForFalse = new Forms.Numeric(
            Forms.Numeric.bind(this, "scoreForFalse", undefined)
        ).label(this.checkboxSlot.labelForFalse || defaultLabelForFalse);

        this.editor.option({
            name: pgettext("block:checkbox", "Labels"),
            form: {
                title: pgettext("block:checkbox", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        this.checkboxSlot.labelForTrue !== defaultLabelForTrue
                            ? this.checkboxSlot.labelForTrue
                            : undefined
                    )
                        .placeholder(defaultLabelForTrue)
                        .on((labelForTrue) => {
                            scoreForTrue.label(
                                (this.checkboxSlot.labelForTrue =
                                    (labelForTrue.isFeatureEnabled &&
                                        labelForTrue.value) ||
                                    defaultLabelForTrue)
                            );
                        }),
                    new Forms.Text(
                        "singleline",
                        this.checkboxSlot.labelForFalse !== defaultLabelForFalse
                            ? this.checkboxSlot.labelForFalse
                            : undefined
                    )
                        .placeholder(defaultLabelForFalse)
                        .on((labelForFalse) => {
                            scoreForFalse.label(
                                (this.checkboxSlot.labelForFalse =
                                    (labelForFalse.isFeatureEnabled &&
                                        labelForFalse.value) ||
                                    defaultLabelForFalse)
                            );
                        }),
                    new Forms.Static(
                        pgettext(
                            "block:checkbox",
                            "These labels will be used in the dataset and override the default values %1 and %2.",
                            `**${defaultLabelForTrue}**`,
                            `**${defaultLabelForFalse}**`
                        )
                    ).markdown(),
                ],
            },
            activated:
                this.checkboxSlot.labelForTrue !== defaultLabelForTrue ||
                this.checkboxSlot.labelForFalse !== defaultLabelForFalse,
        });

        this.editor.scores({
            target: this,
            scores: [scoreForTrue, scoreForFalse],
        });

        this.editor.alias(this.checkboxSlot);
        this.editor.exportable(this.checkboxSlot);
    }

    @conditions
    defineConditions(): void {
        each(
            [
                {
                    checked: true,
                    label:
                        this.checkboxSlot.labelForTrue ||
                        pgettext("block:checkbox", "Checkbox is checked"),
                    icon: ICON_CHECKED,
                },
                {
                    checked: false,
                    label:
                        this.checkboxSlot.labelForFalse ||
                        pgettext("block:checkbox", "Checkbox is not checked"),
                    icon: ICON_UNCHECKED,
                },
            ],
            (condition) => {
                this.conditions.template({
                    condition: CheckboxCondition,
                    label: condition.label,
                    icon: condition.icon,
                    burst: true,
                    props: {
                        slot: this.checkboxSlot,
                        checked: condition.checked,
                    },
                });
            }
        );

        const score = this.slots.select("score", "feature");

        if (score && score.label) {
            const group = this.conditions.group(
                score.label,
                ICON_SCORE,
                false,
                true
            );

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext("block:checkbox", "Score is equal to"),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:checkbox",
                            "Score is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:checkbox",
                            "Score is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:checkbox",
                            "Score is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext("block:checkbox", "Score is between"),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:checkbox",
                            "Score is not between"
                        ),
                    },
                    {
                        mode: "defined",
                        label: pgettext(
                            "block:checkbox",
                            "Score is calculated"
                        ),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:checkbox",
                            "Score is not calculated"
                        ),
                    },
                ],
                (condition: { mode: TScoreModes; label: string }) => {
                    group.template({
                        condition: ScoreCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: score,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }
    }
}
